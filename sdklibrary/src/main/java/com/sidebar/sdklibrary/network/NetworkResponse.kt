package com.sidebar.sdklibrary.network


import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonSyntaxException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NetworkResponse<ResponseType>(private val listener: NetworkResponseListener<ResponseType>, private val type: Int, private val activity: AppCompatActivity?, private val fragment: Fragment?) : Callback<ResponseType> {
    private var error = ""
    private  val  _CONFLICT_ = Constants.HTTPCODES._CONFLICT_
    private val  _CREATED_ = Constants.HTTPCODES._CREATED_
    private  val _OK_ = Constants.HTTPCODES._OK_

    private fun sendSuccessCreateConflict(response: Response<ResponseType>) {

        if (response.code() == _OK_ || response.code() == _CREATED_ || response.code() == _CONFLICT_) {

            response.body()?.let { listener.onResponseReceived(it) }

        } else {

            val errorHandler = RequestErrorHandler(response.errorBody())
            error = errorHandler.errorString

            if (error == "") {

                listener.onError(response.code(), error)

            } else {

                listener.onError(response.code(), error)

            }

        }

    }

    private fun sendSuccessCreate(response: Response<ResponseType>) {

        if (response.code() == _OK_ || response.code() == _CREATED_) {

            response.body()?.let { listener.onResponseReceived(it) }

        } else {

            val errorHandler = RequestErrorHandler(response.errorBody())
            error = errorHandler.errorString

            if (error == "") {

                listener.onError(response.code(), error)

            } else {

                listener.onError(response.code(), error)

            }

        }

    }

    private fun sendSuccess(response: Response<ResponseType>) {

        if (response.code() == _OK_) {

            response.body()?.let { listener.onResponseReceived(it) }

        } else {

            val errorHandler = RequestErrorHandler(response.errorBody())
            error = errorHandler.errorString

            if (error == "") {

                listener.onError(response.code(), error)

            } else {

                listener.onError(response.code(), error)

            }

        }

    }

    private fun sendFail(t: Throwable) {

        if (t is JsonSyntaxException) {

            listener.onEmptyResponse(null)

        } else {

            listener.onError(-1, error)

        }

    }

    private fun successLogic(response: Response<ResponseType>) {

        if (type == 1) {

            sendSuccess(response)

        } else if (type == 2) {

            sendSuccessCreate(response)

        } else {

            sendSuccessCreateConflict(response)

        }

    }

    override fun onResponse(call: Call<ResponseType>, response: Response<ResponseType>) {

        if (activity != null) {

            if (!activity.isFinishing) {

                successLogic(response)

            }

        } else if (fragment != null) {

            if (fragment.isAdded) {

                successLogic(response)

            }

        } else if (activity == null && fragment == null){

            successLogic(response)

        }

    }

    override fun onFailure(call: Call<ResponseType>, t: Throwable) {

        if (activity != null) {

            if (!activity.isFinishing) {

                sendFail(t)

            }

        } else if (fragment != null) {

            if (fragment.isAdded) {

                sendFail(t)

            }

        }

    }

}

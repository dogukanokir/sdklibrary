package com.sidebar.sdklibrary.network

import android.content.res.Resources
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.sidebar.sdklibrary.BuildConfig

import java.util.concurrent.TimeUnit

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

class RequestCreator private constructor(baseUrl: String) {

    private var retrofit: Retrofit? = null

    private val objectMapping: Gson
        get() = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create()

    init {
        setDefaultRetrofit(null, baseUrl)
    }

    private fun setDefaultRetrofit(interceptor: Interceptor?, baseUrl: String) {
        val client = getOkHttpClient(interceptor)
        retrofit = getRetrofit(baseUrl, client)
    }

    private fun getOkHttpClient(interceptor: Interceptor?): OkHttpClient {
        val builder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(loggingInterceptor)
        }
        builder.addInterceptor { chain ->
            val request = chain.request().newBuilder().addHeader("X-localization", Resources.getSystem().configuration.locale.language).build()
            chain.proceed(request)
        }
        builder.addInterceptor { chain ->
            var deviceType = ""
            if (rContext != null){

            }
            var userId = ""
            val request = chain.request().newBuilder().addHeader("User-Agent", BuildConfig.VERSION_NAME + " " + deviceType + " " + userId).build()


            chain.proceed(request)
        }
        val connectivityInterceptor = ConnectivityInterceptor()
        builder.addInterceptor(connectivityInterceptor)

        if (interceptor != null) {
            builder.addInterceptor(interceptor)
        }

        return builder
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build()
    }

    private fun getRetrofit(baseUrl: String, client: OkHttpClient): Retrofit {
        val factory = GsonConverterFactory.create(objectMapping)

        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(factory)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
    }

    companion object {

        fun <T> create(service: Class<T>, baseUrl: String): T {
            return getInstance(baseUrl).retrofit!!.create(service)
        }

        private var Instance: RequestCreator? = null

        private fun getInstance(baseUrl: String): RequestCreator {

            Instance = RequestCreator(baseUrl)

            return Instance as RequestCreator
        }
    }


}


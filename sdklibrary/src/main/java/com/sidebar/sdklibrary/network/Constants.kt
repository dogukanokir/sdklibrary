package com.sidebar.sdklibrary.network

import java.util.*

interface Constants {

    interface NETWORKADDRESS {
        companion object {

            val sideBarUrl = "https://visionapi.metodbox.com/api/"

        }

    }

    interface HTTPCODES {

        companion object {

            val _OK_ = 200
            val _BAD_REQUEST_ = 400
            val _UPDATE_APP_ = 405
            val _CONFLICT_ = 409
            val _CREATED_ = 201

        }
    }

}
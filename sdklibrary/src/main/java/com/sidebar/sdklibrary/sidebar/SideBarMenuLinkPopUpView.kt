package com.sidebar.sdklibrary.sidebar

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.sidebar.sdklibrary.R
import com.sidebar.sdklibrary.response.SideBarResponse
import com.sidebar.sdklibrary.sidebar.AllViewProperties.SideBarMenuLinkPopUpProperties


class SideBarMenuLinkPopUpView(context: Context,var activity: AppCompatActivity?, var mainSideBarMenu: MainSideBarMenu?) : RelativeLayout(context) {

    private var divider: View? = null
    private var icon: ImageView? = null
    private var title: TextView? = null
    private var subTitle: TextView? = null
    private var mainLayout: RelativeLayout? = null
    var userAgreementTrueArray: ArrayList<String>? = null

    init {

        val v = LayoutInflater.from(context).inflate(R.layout.layout_sidebar_menu_link_popup_view,this)

        divider = v.findViewById(R.id.divider)
        icon = v.findViewById(R.id.icon)
        title = v.findViewById(R.id.title)
        subTitle = v.findViewById(R.id.subTitle)
        mainLayout = v.findViewById(R.id.mainLayout)



        setUI()

    }


    fun setUI(){

        //title?.setTextColor(resources.getColor(R.color.ugur_calendar))
        //subTitle?.setTextColor(resources.getColor(R.color.ugur_calendar))

    }

    fun setProperties(properties: SideBarMenuLinkPopUpProperties?){

        if (properties?.titleTypeface != null){

            this.title?.typeface = properties.titleTypeface

        }

        if (properties?.subTitleTypeface != null){

            this.subTitle?.typeface = properties.subTitleTypeface

        }

        if (properties?.titleColor != null){
            this.title?.setTextColor(properties.titleColor!!)
        }

        if (properties?.subTitleColor != null){
            this.subTitle?.setTextColor(properties.subTitleColor!!)
        }

        if (properties?.lineViewColor != null){

            divider?.setBackgroundColor(properties.lineViewColor!!)

        }

    }


    fun setData(data: SideBarResponse?){

        //datayı kontrol et sözleşme button id si geldiyse ekrana bas gelmediyse gösterme


        this.title?.text = data?.title
        this.subTitle?.text = data?.summary

        var uri = Uri.parse(data?.iconURL)

        GlideToVectorYou.justLoadImage(activity,uri,icon)

        mainLayout?.setOnClickListener {

            if (data?.typeStr?.toLowerCase().equals("link")){

                val triggerString = data?.link?.split(":")
                if (triggerString?.get(1).equals("null")){

                }else{
                    mainSideBarMenu?.sideBarToastPopUpp(triggerString?.get(1),null)
                }

            }else{
                mainSideBarMenu?.sideBarMenu(data?.hydraMemberID)
            }



        }

    }

    fun lineViewColor(color: String){

        divider?.setBackgroundColor(Color.parseColor(color))

    }

    fun lineVisibilityGone(){
        divider?.visibility = View.GONE
    }


}


package com.sidebar.sdklibrary.sidebar

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.os.CountDownTimer
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.dynamicanimation.animation.DynamicAnimation
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.dynamicanimation.animation.SpringForce
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.sidebar.sdklibrary.R
import com.sidebar.sdklibrary.controller.SideBarController
import com.sidebar.sdklibrary.response.HydraMemberTrigger
import com.sidebar.sdklibrary.response.SideBarResponse
import com.sidebar.sdklibrary.response.TriggerSideBarResponse
import com.sidebar.sdklibrary.sidebar.AllViewProperties.*
import com.sidebar.sdklibrary.sidebar.PopUpToast.SideBarPopUp
import com.sidebar.sdklibrary.sidebar.PopUpToast.SideBarToastView
import com.sidebar.sdklibrary.sidebar.PopUpToast.SideBarVideoAndLiveSubjectsPopUp
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainSideBarMenu: RelativeLayout, SideBarController.SideBarListener, SideBarController.TriggerSideBarListener {

    companion object{

        var sideMenuContext:Context? = null

    }

    var activity = AppCompatActivity()
    private var titleView: LinearLayout? = null
    private var contentView: LinearLayout? = null
    private var liveSubjectsView: LinearLayout? = null
    private var searchView: SearchView? = null
    private var titleLayout: LinearLayout? = null
    private var titleIcon: ImageView? = null
    private var iconName: TextView? = null
    var emptyText: TextView? = null
    var sideBarMenuIdArray : ArrayList<Long>? = null
    private var sideMenuView: LinearLayout? = null
    var sideBarMenuLinkPopUpProperties: SideBarMenuLinkPopUpProperties? = null
    var sideBarAccordionContentSummaryProperties: SideBarAccordionContentSummaryProperties? = null
    var sideBarAccordionProperties: SideBarAccordionProperties? = null
    var sideBarMainTitleProperties: SideBarMainTitleProperties? = null
    var sideBarSingleAndDoubleColumnProperties: SideBarSingleAndDoubleColumnProperties? = null
    var sideBarSingleDoubleColumnContentImageProperties: SideBarSingleDoubleColumnContentImageProperties? = null
    var sideBarAccordionContentHtmlProperties: SideBarAccordionContentHtmlProperties? = null
    var sideBarPopUpProperties: SideBarPopUpProperties? = null
    var sideBarToastProperties: SideBarToastProperties? = null
    var sideBarController: SideBarController? = null
    var sideBarMenuClickControl = false


    var sideBarVideoAndLiveSubjectsPopUp : SideBarVideoAndLiveSubjectsPopUp? = null
    var sideBarPopUp: SideBarPopUp? = null
    var mainAllProperties: MainAllProperties? = null
    var ad: Dialog? = null
    var sideBarToastPopUp = false
    var old: String? = null
    var new: String? = null
    var fontTitle : Typeface? = null
    var fontSubTitle : Typeface? = null
    var mainLayout: LinearLayout? = null
    var scrollView: ScrollView? = null

    var sideBarMemberList: ArrayList<ArrayList<SideBarResponse>>? = null
    var sideBarTriggerDataToastPopUp: TriggerSideBarResponse? = null
    var sideBarTriggerDataCloseToastArray = java.util.ArrayList<HydraMemberTrigger>()



    constructor(context: Context?) : super(context) {
        init(context!!)

    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)

    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)

    }


    fun init(mainContext: Context) {
        sideMenuContext = mainContext

        val view = LayoutInflater.from(sideMenuContext).inflate(R.layout.layout_sidebar_main_menu,this)

        titleView = view.findViewById(R.id.titleView)
        contentView = view.findViewById(R.id.contentView)

        mainLayout = view.findViewById(R.id.mainLayout)
        searchView = view.findViewById(R.id.searchView)
        titleLayout = view.findViewById(R.id.titleLayout)
        emptyText = view.findViewById(R.id.emptyText)
        scrollView = view.findViewById(R.id.scrollView)

        titleIcon = view.findViewById(R.id.titleIcon)
        iconName = view.findViewById(R.id.iconName)

        sideBarVideoAndLiveSubjectsPopUp = SideBarVideoAndLiveSubjectsPopUp(sideMenuContext!!,this)
        sideBarPopUp = SideBarPopUp(sideMenuContext!!,this)

        sideBarMenuIdArray = ArrayList()
        sideBarMemberList = ArrayList()

    }

    fun staticProperties() {
        sideBarAccordionContentSummaryProperties = SideBarAccordionContentSummaryProperties(ContextCompat.getColor(sideMenuContext!!,R.color.black_color),null,ContextCompat.getColor(sideMenuContext!!,R.color.black_color),null,ContextCompat.getColor(sideMenuContext!!,R.color.black_color))
        sideBarAccordionProperties = SideBarAccordionProperties(ContextCompat.getDrawable(sideMenuContext!!,R.drawable.accordion_black),ContextCompat.getDrawable(sideMenuContext!!,R.drawable.accordion_grey),Gravity.LEFT,ContextCompat.getColor(sideMenuContext!!,R.color.white_color),ContextCompat.getColor(sideMenuContext!!,R.color.black_color),R.mipmap.down_white,R.mipmap.up_black,true,null,ContextCompat.getColor(sideMenuContext!!,R.color.black_color))
        sideBarMainTitleProperties = SideBarMainTitleProperties(ContextCompat.getColor(sideMenuContext!!,R.color.black_color),null,null)
        sideBarMenuLinkPopUpProperties = SideBarMenuLinkPopUpProperties(ContextCompat.getColor(sideMenuContext!!,R.color.black_color),null,ContextCompat.getColor(sideMenuContext!!,R.color.black_color),null,ContextCompat.getColor(sideMenuContext!!,R.color.black_color))
        sideBarSingleAndDoubleColumnProperties = SideBarSingleAndDoubleColumnProperties(ContextCompat.getColor(sideMenuContext!!,R.color.black_color),null,ContextCompat.getColor(sideMenuContext!!,R.color.black_color))
        sideBarSingleDoubleColumnContentImageProperties = SideBarSingleDoubleColumnContentImageProperties(ContextCompat.getColor(sideMenuContext!!,R.color.black_color),null,ContextCompat.getColor(sideMenuContext!!,R.color.black_color))
        sideBarAccordionContentHtmlProperties = SideBarAccordionContentHtmlProperties(ContextCompat.getColor(sideMenuContext!!,R.color.black_color))
        sideBarPopUpProperties = SideBarPopUpProperties(ContextCompat.getColor(sideMenuContext!!,R.color.grey_color),150,150,150,150,"#000000","#ffffff",Gravity.RIGHT,null,R.mipmap.close_white,30,R.color.popup_bg_color)
        sideBarToastProperties = SideBarToastProperties(null,null,R.color.black_color,R.color.black_color,R.color.black_color)
        mainAllProperties = MainAllProperties(sideBarMainTitleProperties,sideBarAccordionProperties,sideBarAccordionContentSummaryProperties,sideBarMenuLinkPopUpProperties,sideBarSingleAndDoubleColumnProperties,sideBarSingleDoubleColumnContentImageProperties,sideBarAccordionContentHtmlProperties,sideBarPopUpProperties,sideBarToastProperties)

    }

    fun sendActivity(activity: AppCompatActivity?,activity2: Activity?){

        if (activity != null){
            this.activity = activity
        }

        if (activity2 != null){
            //this.activity = activity2
        }

    }

    fun setAllViewsProperties(mainAllProperties: MainAllProperties?){

        this.mainAllProperties = mainAllProperties

    }

    fun clearSideBar(){

        val count = object: CountDownTimer(300,300){
            override fun onFinish() {
                titleView?.removeAllViews()
                contentView?.removeAllViews()
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }
        count.start()

    }

    fun sideBarMenuFirstApiCall(){

        sideBarMenuClickControl = false
        sideBarMenuIdArray = ArrayList()
        sideBarMemberList = ArrayList()
        sideMenuView?.removeAllViews()

        sideBarController = SideBarController(activity,null,"bb76cf3d-306d-30f0-b9d9-53d0e131d5d7","collections:mobile",0,null,false,null,null,"asc",10,this,this)
        sideBarController?.sideBar()

    }


    fun titleLayoutVisibilityOrSendData(visibility: Boolean?,titleIcon: Int?,iconName: String?,backgroundColor: Int?,nameTypeface: Typeface?){

        if (visibility != null){

            if (visibility == true){

                titleLayout?.visibility = View.VISIBLE

                if (titleIcon != null){

                    this.titleIcon?.background = resources.getDrawable(titleIcon)

                }

                if (iconName != null){

                    this.iconName?.text = iconName

                }

                if (nameTypeface != null){

                    this.iconName?.typeface = nameTypeface

                }

                if (backgroundColor != null){

                    titleLayout?.setBackgroundColor(ContextCompat.getColor(context,backgroundColor))

                }

            }else{

                titleLayout?.visibility = View.GONE

            }

        }




    }


    fun setTitleData(sideBarMenu: ArrayList<SideBarResponse>?){

        titleView?.removeAllViews()

        if (sideBarMemberList.isNullOrEmpty() || sideBarMemberList?.size == 1){

            if (sideBarMenu == null){

                emptyText?.visibility = View.VISIBLE

            }else{

                val mainView = SideBarMainTitleView(context,activity,this)
                mainView.setTitle("ANASAYFA")
                mainView.setProperties(mainAllProperties?.sideBarMainTitleProperties)
                titleView?.addView(mainView)

            }

        }else{

            val mainView = SideBarMainTitleView(context,activity,this)
            mainView.setTitle(sideBarMenu?.get(0)?.parentCol?.title)
            mainView.setProperties(mainAllProperties?.sideBarMainTitleProperties)
            mainView.setOpenBack()
            titleView?.addView(mainView)

        }

    }


    fun setData(sideBarMenu: ArrayList<SideBarResponse>?, mainAllProperties: MainAllProperties?){

        contentView?.removeAllViews()

        if (sideBarMenu != null){

            for (i in 0..sideBarMenu.size-1){

                when(sideBarMenu.get(i).typeStr){

                    "link" -> {

                        val view = SideBarMenuLinkPopUpView(context,activity,this)
                        view.setProperties(mainAllProperties?.sideBarMenuLinkPopUpProperties)
                        view.setData(sideBarMenu.get(i))
                        contentView?.addView(view)

                    }

                    "menu" -> {

                        val view = SideBarMenuLinkPopUpView(context,activity,this)
                        view.setProperties(mainAllProperties?.sideBarMenuLinkPopUpProperties)
                        view.setData(sideBarMenu.get(i))
                        contentView?.addView(view)

                    }
                    "popup" -> {

                        val view = SideBarMenuLinkPopUpView(context,activity,this)
                        view.setProperties(mainAllProperties?.sideBarMenuLinkPopUpProperties)
                        view.setData(sideBarMenu.get(i))
                        contentView?.addView(view)

                    }

                    "single_column" -> {

                        val view = SideBarSingleAndDoubleColumnView(context,activity,this)
                        view.setProperties(mainAllProperties?.sideBarSingleAndDoubleColumnProperties)
                        view.setTitle(sideBarMenu.get(i).title)
                        view.setDataSideBarMenu(sideBarMenu.get(i).collectionSections,mainAllProperties)
                        contentView?.addView(view)

                    }

                    "double_column" -> {

                        val view = SideBarSingleAndDoubleColumnView(context,activity,this)
                        view.setProperties(mainAllProperties?.sideBarSingleAndDoubleColumnProperties)
                        view.setTitle(sideBarMenu.get(i).title)
                        view.setDataSideBarMenu(sideBarMenu.get(i).collectionSections,mainAllProperties)
                        contentView?.addView(view)

                    }

                    "accordion" -> {

                        val view = SideBarAccordionView(context,this)
                        view.setProperties(mainAllProperties?.sideBarAccordionProperties)
                        view.accordionTitleText(sideBarMenu.get(i).title)
                        view.contentData(sideBarMenu.get(i).collectionSections, mainAllProperties)
                        contentView?.addView(view)

                    }



                }


            }

        }

    }

    fun sideBarToastPopUpp(triggerName: String?,androidLink: String?){

        sideBarTriggerDataCloseToastArray = ArrayList()
        sideBarController?.triggerSideBar(triggerName,"triggers:mobile")

    }

    fun sideBarToastPopUpBase(anyOneActivity: AppCompatActivity?){

        //activity.toastLayout?.removeAllViews()

        if (sideBarTriggerDataToastPopUp == null){

            sideBarToastPopUp = false

        }else{

            if (sideBarTriggerDataToastPopUp?.hydraMember?.size!! > 0 && sideBarTriggerDataToastPopUp != null){

                sideBarToastPopUp = true

            }else{

                sideBarToastPopUp = false

            }

        }

        if (sideBarToastPopUp == true){

            for (i in 0..sideBarTriggerDataToastPopUp?.hydraMember?.size!!-1){

                openToastCloseSideBar()

                if (i == 0){

                    activity.toastLayout.visibility = View.VISIBLE

                    //activity.toastLayout?.removeAllViews()

                    //openToastCloseSideBar()
                }

                val view = SideBarToastView(context, activity, this)
                view.setProperties(mainAllProperties?.sideBarToastProperties)

                view.closeIcon?.setOnClickListener {

                    sideBarTriggerDataCloseToastArray.add(sideBarTriggerDataToastPopUp?.hydraMember?.get(i)!!)

                    if (activity.toastLayout?.childCount!! > 1){

                        activity.toastLayout?.removeView(view)

                    }else{
                        activity.toastLayout?.removeView(view)

                        closeToast(sideBarTriggerDataToastPopUp)

                    }

                    animIn()

                    /*if (activity is MainPageActivity){

                        (activity as MainPageActivity).openToastLayout()

                    }else if (activity is GMActivity){

                        (activity as GMActivity).openToastLayout()

                    }else if (activity is GuardianActivity){

                        (activity as GuardianActivity).openToastLayout()

                    }else if (activity is JuniorActivity){

                        (activity as JuniorActivity).openToastLayout()

                    }*/

                }

                view.setTitleAndSubTitle(sideBarTriggerDataToastPopUp?.hydraMember?.get(0)?.result?.title,sideBarTriggerDataToastPopUp?.hydraMember?.get(0)?.result?.content,sideBarTriggerDataToastPopUp?.hydraMember?.get(0)?.result?.viewTypeStr)

                view.setOnClickListener {

                    animIn()

                    if (activity.toastLayout?.childCount!! > 0){

                        sideBarTriggerDataCloseToastArray.add(sideBarTriggerDataToastPopUp?.hydraMember?.get(i)!!)
                        activity.toastLayout?.removeView(view)

                    }else{

                        activity.toastLayout?.visibility = View.GONE

                    }

                    //activity.toastLayout?.visibility = View.GONE
                    sideBarTriggerDataToastPopUp = null

                }

                if (sideBarTriggerDataCloseToastArray.size > 0){

                    var closeControl = false

                    for (j in 0..sideBarTriggerDataCloseToastArray.size-1){

                        if (sideBarTriggerDataToastPopUp?.hydraMember?.get(i) == sideBarTriggerDataCloseToastArray.get(j)){

                            closeControl = true

                        }

                    }

                    if (closeControl != true){

                        activity.toastLayout?.addView(view)

                    }

                }else{

                    activity.toastLayout?.addView(view)

                }

            }

        }else{



        }

    }


    fun sideBarMenu(id: Long?){

        sideBarMenuIdArray?.add(id!!)

        sideBarMenuClickControl = true

        var filterData = JsonObject()

        //filterData = filterDataReturn()

        sideBarController = SideBarController(activity,null,"bb76cf3d-306d-30f0-b9d9-53d0e131d5d7","collections:mobile",0,id,null,null,null,"asc",10,this,this)

        sideBarController?.sideBar()

    }

    fun openToastCloseSideBar(){

        animOut()

    }

    fun closeToast(sideBarTriggerDataToastPopup: TriggerSideBarResponse?){

        if (sideBarTriggerDataToastPopup != null){

        }else{

            activity.toastLayout?.visibility = View.GONE

        }

        if (sideBarTriggerDataToastPopup != null){
            sideBarTriggerDataToastPopUp = null
        }

        ad?.dismiss()

    }

    override fun triggerSideBarListener(responseOk: Boolean, jsonElement: JsonElement?, failMessage: Int, error: String) {

        if (responseOk){

            if (jsonElement != null){

                var triggerSideBar = Gson().fromJson(jsonElement, TriggerSideBarResponse::class.java)

                if (triggerSideBar.hydraMember?.size!! > 0){

                    if (triggerSideBar.hydraMember?.get(0)?.result != null){

                        if (triggerSideBar.hydraMember?.get(0)?.result?.typeStr != null){

                            if (triggerSideBar.hydraMember?.get(0)?.result?.typeStr.equals("modal")){

                                //PopUp

                                sideBarPopUp?.setPropertiesPopUp(mainAllProperties?.sideBarPopUpProperties)
                                sideBarPopUp?.setVariables(triggerSideBar,triggerSideBar.hydraMember?.get(0)?.result?.typeStr,triggerSideBar.hydraMember?.get(0)?.result?.viewTypeStr,triggerSideBar?.hydraMember?.get(0)?.result?.linkAndroid)

                                ad = sideBarPopUp

                                ad?.show()
                                ad?.setCanceledOnTouchOutside(true)

                            }else{

                                //Toast

                                sideBarTriggerDataToastPopUp = triggerSideBar

                                sideBarToastPopUpBase(activity)

                            }

                        }

                    }

                }else{

                    Toast.makeText(activity,"İçerik Bulunmamaktadır.", Toast.LENGTH_LONG).show()

                }

            }else{

                Toast.makeText(activity,"İçerik Bulunmamaktadır.", Toast.LENGTH_LONG).show()

            }

        }else{

            Toast.makeText(activity,"Servis ile ilgili bir hata oluşmuştur.", Toast.LENGTH_LONG).show()

        }

    }

    fun sideBarTopBack(){

        var lastData = sideBarMemberList?.size?.minus(1)

        if (sideBarMenuClickControl){

            if (sideBarMenuIdArray?.size == 1){
                sideBarMenuClickControl = false
                sideBarMenuIdArray = ArrayList()
            }else{
                sideBarMenuIdArray?.removeAt(sideBarMenuIdArray?.size!!-1)
            }
        }

        sideBarMemberList?.removeAt(lastData!!)

        sideMenuView?.removeAllViews()

        var lastData2 = sideBarMemberList?.size?.minus(1)

        var sideBarResponse = sideBarMemberList?.get(lastData2!!)

        if (sideBarMemberList?.size != 0){
            setTitleData(sideBarResponse)
            setData(sideBarResponse,mainAllProperties)
        }

    }

    override fun sideBarListener(responseOk: Boolean, jsonArray: JsonArray?, failMessage: Int, error: String) {

        if (responseOk){

            if (jsonArray != null){

                val collectionType = object : TypeToken<ArrayList<SideBarResponse>>(){}.type

                var sideBarMenu = ArrayList<SideBarResponse>()

                sideBarMenu = Gson().fromJson(jsonArray,collectionType)

                if (sideBarMenu.isNullOrEmpty()){
                    Toast.makeText(activity,"İçerik Bulunmamaktadır.", Toast.LENGTH_LONG).show()
                }else{

                    sideMenuView?.removeAllViews()

                    sideBarMemberList?.add(sideBarMenu)

                    setTitleData(sideBarMenu)
                    setData(sideBarMenu,mainAllProperties)

                    scrollView?.scrollTo(0,0)

                }

            }else{


            }

        }else{

            setData(null,mainAllProperties)

        }

    }

    fun animIn(){

        this.visibility= View.VISIBLE

        val count = object: CountDownTimer(500,500){
            override fun onFinish() {
                //sideMenuFrame?.setBackgroundColor(Color.parseColor("#B3C297CE"))
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }
        count.start()

        mainLayout?.startAnimation(inFromRightAnimation())
        SpringAnimation(sideMenuView, DynamicAnimation.TRANSLATION_X, 0f).apply {
            this.spring.dampingRatio =  SpringForce.DAMPING_RATIO_LOW_BOUNCY
        }
        sideMenuView?.visibility = View.VISIBLE

    }

    fun animOut(){

        mainLayout?.startAnimation(outToRightAnimation())

        val count = object: CountDownTimer(500,500){
            override fun onFinish() {
                visibility= View.GONE
            }

            override fun onTick(millisUntilFinished: Long) {

            }

        }
        count.start()

    }

    fun outToRightAnimation(): Animation {
        val outtoRight = TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f)
        outtoRight.duration = 500

        outtoRight.interpolator = AccelerateInterpolator()
        return outtoRight
    }

    fun inFromRightAnimation(): Animation {
        val inFromRight = TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f)
        inFromRight.duration = 500
        inFromRight.interpolator = AccelerateInterpolator()

        return inFromRight
    }


    /*fun pageNameForFilter(pageName: String?){

        this.pageName = pageName

    }

    fun pageNameNull(){

        pageName = null

    }

    fun filterDataReturn() : JsonObject{

        var filterData = JsonObject()

        if(GlobalVars.isBK){

            filterData.addProperty("schoolChain",2)

        }else if(GlobalVars.isUgur){

            filterData.addProperty("schoolChain",1)

        }

        if(GlobalVars.loginResponse?.userType?.toLowerCase().equals("student")){

            filterData.addProperty("userRole",1)

        }else if(GlobalVars.loginResponse?.userType?.toLowerCase().equals("teacher")){

            filterData.addProperty("userRole",2)

        }else if(GlobalVars.loginResponse?.userType?.toLowerCase().equals("manager")){

            filterData.addProperty("userRole",3)

        }else if(GlobalVars.loginResponse?.userType?.toLowerCase().equals("gm")){

            filterData.addProperty("userRole",4)

        }else if(GlobalVars.loginResponse?.userType?.toLowerCase().equals("guardian")){

            filterData.addProperty("userRole",5)

        }

        if(userAgreementTrueArray?.size == 0){

            filterData.addProperty("userContractType",1)

        }else if(userAgreementTrueArray?.size!! > 0){

            var AMETNI_V1 = false
            var PRIVACY_TERM_V1 = false
            var PRIVACY_TERM_V2 = false

            for(i in 0..userAgreementTrueArray?.size!!-1){

                if (userAgreementTrueArray?.get(i)?.equals("AMETNI_V1")!!) {
                    AMETNI_V1 = true
                } else if (userAgreementTrueArray?.get(i)?.equals("PRIVACY_TERM_V1")!!) {
                    PRIVACY_TERM_V1 = true
                } else if (userAgreementTrueArray?.get(i)?.equals("PRIVACY_TERM_V2")!!) {
                    PRIVACY_TERM_V2 = true
                }

            }

            if (AMETNI_V1 && PRIVACY_TERM_V1 && PRIVACY_TERM_V2) {

                filterData.addProperty("userContractType",8)

            } else if (PRIVACY_TERM_V1 && PRIVACY_TERM_V2) {

                filterData.addProperty("userContractType",7)

            } else if (AMETNI_V1 && PRIVACY_TERM_V1) {

                filterData.addProperty("userContractType",6)

            } else if (AMETNI_V1 && PRIVACY_TERM_V2) {

                filterData.addProperty("userContractType",5)

            } else if (PRIVACY_TERM_V1) {

                filterData.addProperty("userContractType",4)

            } else if (PRIVACY_TERM_V2) {

                filterData.addProperty("userContractType",3)

            } else if (AMETNI_V1) {

                filterData.addProperty("userContractType",2)

            }

        }

        if(GlobalVars.loginResponse?.userType?.toLowerCase().equals("student")){

            if (GlobalVars.loginResponse?.gradeName?.equals("İlkokul 1")!! || GlobalVars.loginResponse?.gradeName?.equals("İlköğretim 1")!!){

                filterData.addProperty("gradeId",1)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("İlkokul 2")!! || GlobalVars.loginResponse?.gradeName?.equals("İlköğretim 2")!!){

                filterData.addProperty("gradeId",2)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("İlkokul 3")!! || GlobalVars.loginResponse?.gradeName?.equals("İlköğretim 3")!!){

                filterData.addProperty("gradeId",3)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("İlkokul 4")!! || GlobalVars.loginResponse?.gradeName?.equals("İlköğretim 4")!!){

                filterData.addProperty("gradeId",4)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Ortaokul 5")!! || GlobalVars.loginResponse?.gradeName?.equals("İlköğretim 5")!!){

                filterData.addProperty("gradeId",5)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Ortaokul 6")!! || GlobalVars.loginResponse?.gradeName?.equals("İlköğretim 6")!!){

                filterData.addProperty("gradeId",6)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Ortaokul 7")!! || GlobalVars.loginResponse?.gradeName?.equals("İlköğretim 7")!!){

                filterData.addProperty("gradeId",7)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Ortaokul 8")!! || GlobalVars.loginResponse?.gradeName?.equals("İlköğretim 8")!!){

                filterData.addProperty("gradeId",8)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Lise 1")!!){

                filterData.addProperty("gradeId",9)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Lise 2")!!){

                filterData.addProperty("gradeId",10)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Lise 3")!!){

                filterData.addProperty("gradeId",11)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Lise 4")!!){

                filterData.addProperty("gradeId",12)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Mezun")!!){

                filterData.addProperty("gradeId",13)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Lise Hazırlık")!!){

                filterData.addProperty("gradeId",14)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Ana Sınıfı")!!){

                filterData.addProperty("gradeId",15)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Anaokulu 2 Yaş")!!){

                filterData.addProperty("gradeId",16)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Anaokulu 3 Yaş")!!){

                filterData.addProperty("gradeId",17)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Anaokulu 4 Yaş")!!){

                filterData.addProperty("gradeId",18)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Anaokulu 5 Yaş")!!){

                filterData.addProperty("gradeId",19)

            }else if (GlobalVars.loginResponse?.gradeName?.equals("Okul Öncesi")!!){

                filterData.addProperty("gradeId",20)

            }else{

                filterData.addProperty("gradeId",0)

            }

        }else if (GlobalVars.loginResponse?.userType?.toLowerCase().equals("teacher")){

            if (GlobalVars.selectGradeName != null){

                if (GlobalVars.selectGradeName.equals("İlkokul 1") || GlobalVars.selectGradeName.equals("İlköğretim 1")){

                    filterData.addProperty("gradeId",1)

                }else if (GlobalVars.selectGradeName.equals("İlkokul 2") || GlobalVars.selectGradeName.equals("İlköğretim 2")){

                    filterData.addProperty("gradeId",2)

                }else if (GlobalVars.selectGradeName.equals("İlkokul 3") || GlobalVars.selectGradeName.equals("İlköğretim 3")){

                    filterData.addProperty("gradeId",3)

                }else if (GlobalVars.selectGradeName.equals("İlkokul 4") || GlobalVars.selectGradeName.equals("İlköğretim 4")){

                    filterData.addProperty("gradeId",4)

                }else if (GlobalVars.selectGradeName.equals("Ortaokul 5") || GlobalVars.selectGradeName.equals("İlköğretim 5")){

                    filterData.addProperty("gradeId",5)

                }else if (GlobalVars.selectGradeName.equals("Ortaokul 6") || GlobalVars.selectGradeName.equals("İlköğretim 6")){

                    filterData.addProperty("gradeId",6)

                }else if (GlobalVars.selectGradeName.equals("Ortaokul 7") || GlobalVars.selectGradeName.equals("İlköğretim 7")){

                    filterData.addProperty("gradeId",7)

                }else if (GlobalVars.selectGradeName.equals("Ortaokul 8") || GlobalVars.selectGradeName.equals("İlköğretim 8")){

                    filterData.addProperty("gradeId",8)

                }else if (GlobalVars.selectGradeName.equals("Lise 1")){

                    filterData.addProperty("gradeId",9)

                }else if (GlobalVars.selectGradeName.equals("Lise 2")){

                    filterData.addProperty("gradeId",10)

                }else if (GlobalVars.selectGradeName.equals("Lise 3")){

                    filterData.addProperty("gradeId",11)

                }else if (GlobalVars.selectGradeName.equals("Lise 4")){

                    filterData.addProperty("gradeId",12)

                }else if (GlobalVars.selectGradeName.equals("Mezun")){

                    filterData.addProperty("gradeId",13)

                }else if (GlobalVars.selectGradeName.equals("Lise Hazırlık")){

                    filterData.addProperty("gradeId",14)

                }else if (GlobalVars.selectGradeName.equals("Ana Sınıfı")){

                    filterData.addProperty("gradeId",15)

                }else if (GlobalVars.selectGradeName.equals("Anaokulu 2 Yaş")){

                    filterData.addProperty("gradeId",16)

                }else if (GlobalVars.selectGradeName.equals("Anaokulu 3 Yaş")){

                    filterData.addProperty("gradeId",17)

                }else if (GlobalVars.selectGradeName.equals("Anaokulu 4 Yaş")){

                    filterData.addProperty("gradeId",18)

                }else if (GlobalVars.selectGradeName.equals("Anaokulu 5 Yaş")){

                    filterData.addProperty("gradeId",19)

                }else if (GlobalVars.selectGradeName.equals("Okul Öncesi")){

                    filterData.addProperty("gradeId",20)

                }else{

                    filterData.addProperty("gradeId",0)

                }


            }

        }



        if (GlobalVars.loginResponse?.branchName?.equals("IO")!!){

            filterData.addProperty("branchId",1)

        }else if (GlobalVars.loginResponse?.gradeName?.equals("AO")!!){

            filterData.addProperty("branchId",2)

        }else if (GlobalVars.loginResponse?.gradeName?.equals("OO")!!){

            filterData.addProperty("branchId",3)

        }else if (GlobalVars.loginResponse?.gradeName?.equals("LO")!!){

            filterData.addProperty("branchId",4)

        }else if (GlobalVars.loginResponse?.gradeName?.equals("L1")!!){

            filterData.addProperty("branchId",5)

        }else if (GlobalVars.loginResponse?.gradeName?.equals("SA")!! && GlobalVars.loginResponse?.gradeName?.equals("MF")!!){

            filterData.addProperty("branchId",6)

        }else if (GlobalVars.loginResponse?.gradeName?.equals("EA")!! && GlobalVars.loginResponse?.gradeName?.equals("TM")!!){

            filterData.addProperty("branchId",7)

        }else if (GlobalVars.loginResponse?.gradeName?.equals("S")!! && GlobalVars.loginResponse?.gradeName?.equals("SO")!!){

            filterData.addProperty("branchId",8)

        }else if (GlobalVars.loginResponse?.gradeName?.equals("YD")!! && GlobalVars.loginResponse?.gradeName?.equals("D")!!){

            filterData.addProperty("branchId",9)

        }else{

            filterData.addProperty("branchId",0)

        }

        return filterData
    }

     */



}
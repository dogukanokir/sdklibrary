package com.sidebar.sdklibrary.sidebar.PopUpToast

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.sidebar.sdklibrary.R
import com.sidebar.sdklibrary.sidebar.AllViewProperties.SideBarToastProperties
import com.sidebar.sdklibrary.sidebar.MainSideBarMenu
import top.defaults.drawabletoolbox.DrawableBuilder

@SuppressLint("ViewConstructor")
class SideBarToastView(context: Context, var activity: AppCompatActivity?, var sideBarMenu: MainSideBarMenu?): RelativeLayout(context) {


    private var icon: ImageView? = null
    private var title: TextView? = null
    private var subTitle: TextView? = null
    var closeIcon: ImageView? = null

    private var mainLayout: LinearLayout? = null


    init {

        val view = LayoutInflater.from(context).inflate(R.layout.layout_sidebar_privacy_policy_view,this)

        icon = view.findViewById(R.id.icon)
        title = view.findViewById(R.id.title)
        subTitle = view.findViewById(R.id.subTitle)
        closeIcon = view.findViewById(R.id.closeIcon)
        mainLayout = view.findViewById(R.id.mainLayout)

    }

    fun setProperties(sideBarToastProperties: SideBarToastProperties?){

        if (sideBarToastProperties?.titleSubTitleTypeface != null){

            title?.typeface = sideBarToastProperties.titleSubTitleTypeface
            subTitle?.typeface = sideBarToastProperties.titleSubTitleTypeface

        }

        if (sideBarToastProperties?.subTitleColor != null){

            subTitle?.setTextColor(ContextCompat.getColor(context, sideBarToastProperties.subTitleColor!!))

        }

        if (sideBarToastProperties?.closeIconColor != null){

            closeIcon?.setColorFilter(ContextCompat.getColor(context,sideBarToastProperties.closeIconColor!!),PorterDuff.Mode.SRC_IN)

        }

        if (sideBarToastProperties?.strokeColor != null && sideBarToastProperties.toastBackgroundColor != null){

            mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,sideBarToastProperties.toastBackgroundColor!!)).strokeColor(ContextCompat.getColor(context,sideBarToastProperties.strokeColor!!)).strokeWidth(3).cornerRadius(30).build()

        }else if (sideBarToastProperties?.strokeColor != null && sideBarToastProperties.toastBackgroundColor == null){

            mainLayout?.background = DrawableBuilder().strokeColor(ContextCompat.getColor(context,sideBarToastProperties.strokeColor!!)).strokeWidth(3).cornerRadius(30).build()

        }else if (sideBarToastProperties?.strokeColor == null && sideBarToastProperties?.toastBackgroundColor != null){

            mainLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,sideBarToastProperties.toastBackgroundColor!!)).cornerRadius(30).build()

        }

    }


    fun setTitleAndSubTitle(title: String?, subTitle: String?,type: String?){

        if (type.equals("general")){

            this.title?.setTextColor(resources.getColor(R.color.general_popup_color))

        }else if (type.equals("warning")){

            this.title?.setTextColor(resources.getColor(R.color.warning_popup_color))

        }else if (type.equals("success")){

            this.title?.setTextColor(resources.getColor(R.color.success_popup_color))

        }

        this.title?.text = title
        this.subTitle?.text = subTitle

    }

}
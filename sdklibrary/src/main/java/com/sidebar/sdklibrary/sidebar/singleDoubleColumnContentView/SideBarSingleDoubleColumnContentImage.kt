package com.sidebar.sdklibrary.sidebar.singleDoubleColumnContentView

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.sidebar.sdklibrary.R
import com.sidebar.sdklibrary.sidebar.AllViewProperties.SideBarSingleDoubleColumnContentImageProperties

class SideBarSingleDoubleColumnContentImage(context: Context): RelativeLayout(context) {


    private var image: ImageView? = null
    private var imageName: TextView? = null


    init {

        val view = LayoutInflater.from(context).inflate(R.layout.layout_sidebar_single_and_doublecolumn_content_image,this)

        image = view.findViewById(R.id.image)
        imageName = view.findViewById(R.id.imageName)


    }


    fun setData(url: String?, name: String?){

        var requestOptions = RequestOptions()
        requestOptions.transform(FitCenter(), RoundedCorners(30))

        Glide.with(context).load(url).apply(requestOptions).into(image!!)
        imageName?.text = name

    }

    fun setProperties(properties: SideBarSingleDoubleColumnContentImageProperties?){

        if (properties?.nameColor != null){

            imageName?.setTextColor(properties.nameColor!!)

        }

        if (properties?.typeface != null){

            imageName?.typeface = properties.typeface

        }

    }


}
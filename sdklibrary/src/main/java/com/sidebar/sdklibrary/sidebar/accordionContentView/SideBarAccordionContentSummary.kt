package com.sidebar.sdklibrary.sidebar.accordionContentView

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.sidebar.sdklibrary.R
import com.sidebar.sdklibrary.sidebar.AllViewProperties.SideBarAccordionContentSummaryProperties

class SideBarAccordionContentSummary(context: Context) : RelativeLayout(context) {


    private var divider: View? = null
    private var title: TextView? = null
    private var subTitle: TextView? = null

    init {

        val view = LayoutInflater.from(context).inflate(R.layout.layout_sidebar_accordion_content_summary,this)

        divider = view.findViewById(R.id.divider)
        title = view.findViewById(R.id.title)
        subTitle = view.findViewById(R.id.subTitle)



    }

    fun lineViewColor(color: String){

        divider?.setBackgroundColor(Color.parseColor(color))

    }

    fun lineVisibilityGone(){
        divider?.visibility = View.GONE
    }

    fun setTitleAndSubTitleData(title: String?,subTitle: String?){

        this.title?.text = title


        this.subTitle?.text = subTitle


    }

    fun setProperties(properties: SideBarAccordionContentSummaryProperties?){

        if (properties?.titleColor != null){
            this.title?.setTextColor(properties.titleColor!!)
        }

        if (properties?.subTitleColor != null){
            this.subTitle?.setTextColor(properties.subTitleColor!!)
        }

        if (properties?.titleTypeface != null){
            this.title?.typeface = properties.titleTypeface
        }

        if (properties?.subTitleTypeface != null){
            this.subTitle?.typeface = properties.subTitleTypeface
        }

        if (properties?.lineViewColor != null){

            divider?.setBackgroundColor(properties.lineViewColor!!)

        }

    }



}
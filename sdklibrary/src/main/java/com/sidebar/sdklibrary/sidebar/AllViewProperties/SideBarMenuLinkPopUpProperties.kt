package com.sidebar.sdklibrary.sidebar.AllViewProperties

import android.graphics.Typeface

data class SideBarMenuLinkPopUpProperties(var titleColor: Int?, var titleTypeface: Typeface?, var subTitleColor: Int?, var subTitleTypeface: Typeface?,var lineViewColor: Int?)
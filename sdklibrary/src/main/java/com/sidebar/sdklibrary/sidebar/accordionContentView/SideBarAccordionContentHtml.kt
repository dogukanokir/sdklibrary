package com.sidebar.sdklibrary.sidebar.accordionContentView

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.sidebar.sdklibrary.R
import com.sidebar.sdklibrary.sidebar.AllViewProperties.MainAllProperties
import com.sidebar.sdklibrary.sidebar.AllViewProperties.SideBarAccordionContentHtmlProperties
import com.sidebar.sdklibrary.sidebar.MainSideBarMenu
import com.sidebar.sdklibrary.sidebar.PopUpToast.SideBarVideoAndLiveSubjectsPopUp

class SideBarAccordionContentHtml(context: Context,var mainSideBarMenu: MainSideBarMenu?): RelativeLayout(context) {


    private var divider: View? = null
    var webView: WebView? = null
    var imageView: ImageView? = null
    var playIcon: ImageView? = null
    var imageViewLayout: RelativeLayout? = null
    var titleText: TextView? = null

    var sideBarVideoAndLiveSubjectsPopUp : SideBarVideoAndLiveSubjectsPopUp? = null


    init {

        val view = LayoutInflater.from(context).inflate(R.layout.layout_sidebar_accordion_content_html,this)

        webView = view.findViewById(R.id.webView)
        divider = view.findViewById(R.id.divider)
        imageView = view.findViewById(R.id.imageView)
        playIcon = view.findViewById(R.id.playIcon)
        imageViewLayout = view.findViewById(R.id.imageViewLayout)
        titleText = view.findViewById(R.id.titleText)

        //webView?.settings?.javaScriptEnabled = true
        //settings?.domStorageEnabled = true
        //settings?.allowFileAccess = true
        //webView?.settings?.loadWithOverviewMode = true
        //webView?.settings?.useWideViewPort = true
        //webView?.settings?.setSupportZoom(true)
        //webView?.settings?.builtInZoomControls = false


        webView?.settings?.javaScriptEnabled = true
        webView?.settings?.loadWithOverviewMode = true
        //webView?.settings?.useWideViewPort = true
        //webView?.settings?.setSupportZoom(false)
        webView?.settings?.builtInZoomControls = false
        webView?.settings?.allowContentAccess = true
        webView?.settings?.domStorageEnabled =true

        webView?.isScrollContainer = true



        webView?.settings?.pluginState = WebSettings.PluginState.ON
        //settings?.setAppCacheEnabled(false)
        //webView?.clearCache(true)
        webView?.webChromeClient = WebChromeClient()
        //webView?.settings?.mediaPlaybackRequiresUserGesture = true

    }

    /*fun sendSideMenu(mainSideBarMenu: MainSideBarMenu?){

        this.mainSideBarMenu = mainSideBarMenu

    }

     */

    fun lineViewColor(color: String){

        divider?.setBackgroundColor(Color.parseColor(color))

    }

    fun lineVisibilityGone(){
        divider?.visibility = View.GONE
    }

    fun setProperties(properties: SideBarAccordionContentHtmlProperties?){

        if (properties?.lineViewColor != null){

            divider?.setBackgroundColor(properties.lineViewColor!!)

        }

    }

    fun setDataWebView(url: String?){

        imageViewLayout?.visibility = View.GONE

        webView?.loadUrl(url)

        var param = webView?.layoutParams
        param?.height = 400

        webView?.layoutParams = param
        /*if(url == null && html != null){
            webView?.loadDataWithBaseURL(null,html,null,null,null)
        }else if(html == null && url != null){
            webView?.loadDataWithBaseURL(url,null,null,null,null)
        }else{

        }*/


    }

    fun setDataPreviewImage(imageUrl: String?,mediaUrl: String?,html: String?,title: String?,mainAllProperties : MainAllProperties?){

        imageViewLayout?.visibility = View.VISIBLE

        if (imageUrl != null){

            //var requestOptions = RequestOptions()
            //requestOptions.transform(FitCenter(), RoundedCorners(30))

            Glide.with(context).load(imageUrl).into(imageView!!)
            //Glide.with(context).load(resources.getDrawable(R.mipmap.icon_video_small)).into(playIcon!!)

        }else{

            //Glide.with(context).load(resources.getDrawable(R.drawable.video_thumb)).into(imageView!!)
            Glide.with(context).load(resources.getDrawable(R.mipmap.icon_video_small)).into(playIcon!!)

        }

        if (title != null){
            titleText?.setTextColor(ContextCompat.getColor(context,R.color.black_color))
            titleText?.text = title
        }


        imageView?.setOnClickListener {

            sideBarVideoAndLiveSubjectsPopUp = SideBarVideoAndLiveSubjectsPopUp(context,mainSideBarMenu)
            sideBarVideoAndLiveSubjectsPopUp?.setPropertiesVideoHtmlPopUp(mainAllProperties?.sideBarPopUpProperties)

            if (html != null){

                sideBarVideoAndLiveSubjectsPopUp?.setHtmlVariables(html,title)

            }else if (mediaUrl != null){

                sideBarVideoAndLiveSubjectsPopUp?.setVariables(mediaUrl,title)

            }

            mainSideBarMenu?.ad = sideBarVideoAndLiveSubjectsPopUp

            mainSideBarMenu?.ad?.show()


            //mainSideBarMenu?.ad?.setCanceledOnTouchOutside(false)

            /*val intent = Intent(context,WebViewActivity::class.java)
            intent.putExtra("urlString",mediaUrl)
            context.startActivity(intent)*/

        }

    }

    fun setHtml(html: String?){

        webView?.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null)

    }



}
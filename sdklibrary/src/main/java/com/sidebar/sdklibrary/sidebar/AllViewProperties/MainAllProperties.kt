package com.sidebar.sdklibrary.sidebar.AllViewProperties


class MainAllProperties(var sideBarMainTitleProperties: SideBarMainTitleProperties?, var sideBarAccordionProperties: SideBarAccordionProperties?
                        , var sideBarAccordionContentSummaryProperties: SideBarAccordionContentSummaryProperties?, var sideBarMenuLinkPopUpProperties: SideBarMenuLinkPopUpProperties?
                        , var sideBarSingleAndDoubleColumnProperties: SideBarSingleAndDoubleColumnProperties?, var sideBarSingleDoubleColumnContentImageProperties: SideBarSingleDoubleColumnContentImageProperties?
                        , var sideBarAccordionContentHtmlProperties: SideBarAccordionContentHtmlProperties?, var sideBarPopUpProperties: SideBarPopUpProperties?
                        , var sideBarToastProperties: SideBarToastProperties?)
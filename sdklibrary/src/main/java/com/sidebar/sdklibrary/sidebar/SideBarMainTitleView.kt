package com.sidebar.sdklibrary.sidebar

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.sidebar.sdklibrary.R
import com.sidebar.sdklibrary.sidebar.AllViewProperties.SideBarMainTitleProperties

class SideBarMainTitleView(context: Context,var activity: AppCompatActivity?,var mainSideBarMenu: MainSideBarMenu?): RelativeLayout(context) {


    private var backIcon: ImageView? = null
    private var mainTitle: TextView? = null


    init {

        val view = LayoutInflater.from(context).inflate(R.layout.layout_sidebar_maintitle_view,this)

        backIcon = view.findViewById(R.id.backIcon)
        mainTitle = view.findViewById(R.id.mainTitle)

    }

    fun setTitle(title: String?){

        mainTitle?.text = title

    }

    fun setProperties(properties: SideBarMainTitleProperties?){

        if (properties?.titleTypeface != null){
            mainTitle?.typeface = properties.titleTypeface
        }

        if (properties?.titleColor != null){
            mainTitle?.setTextColor(properties.titleColor!!)
        }

        if (properties?.iconColor != null){
            backIcon?.setColorFilter(properties.iconColor!!, PorterDuff.Mode.SRC_ATOP)
        }

    }

    fun setOpenBack(){

        backIcon?.visibility = View.VISIBLE

        backIcon?.setOnClickListener {

            mainSideBarMenu?.sideBarTopBack()

        }

    }


}
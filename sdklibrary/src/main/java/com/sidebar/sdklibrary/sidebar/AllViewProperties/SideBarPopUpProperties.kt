package com.sidebar.sdklibrary.sidebar.AllViewProperties

data class SideBarPopUpProperties(var popUpMainBackgroundColor: Int?,var popupLayoutLeftMargin: Int,var popupLayoutRightMargin: Int,var popupLayoutTopMargin: Int,
                                  var popupLayoutBottomMargin: Int,var headerTitleBackgroundColor: String?,var headerTitleColor: String?,var headerTitleGravity: Int?,
                                  var headerTitleIconLeft: Int?,var headerTitleIconRight: Int?,var radius: Int?,var backgroundColor: Int?)
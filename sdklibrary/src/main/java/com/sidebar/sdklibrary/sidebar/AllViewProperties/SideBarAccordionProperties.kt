package com.sidebar.sdklibrary.sidebar.AllViewProperties

import android.graphics.drawable.Drawable

class SideBarAccordionProperties(var drawableBgClose: Drawable?, var drawableBgOpen: Drawable?, var titleGravity: Int?, var titleColorOpen: Int?, var titleColorClose: Int?, var iconClose: Int?, var iconOpen: Int?, var left: Boolean?, var right: Boolean?, var lineViewColor: Int?)
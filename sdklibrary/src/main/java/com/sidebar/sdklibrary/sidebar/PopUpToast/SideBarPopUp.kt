package com.sidebar.sdklibrary.sidebar.PopUpToast

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.webkit.WebView
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.view.marginLeft
import androidx.core.view.setMargins
import com.sidebar.sdklibrary.R
import com.sidebar.sdklibrary.response.TriggerSideBarResponse
import com.sidebar.sdklibrary.sidebar.AllViewProperties.SideBarPopUpProperties
import com.sidebar.sdklibrary.sidebar.MainSideBarMenu
import top.defaults.drawabletoolbox.DrawableBuilder


class SideBarPopUp(context: Context,var mainSideBarMenu: MainSideBarMenu?): Dialog(context, R.style.Theme_AppCompat_Light_Dialog) {

    private var headerTextViewPopUp: TextView? = null
    private var alertView: RelativeLayout? = null
    private var headerTextColor = Color.WHITE
    private var backgroundLayout: RelativeLayout? = null
    private var bodyLayout: LinearLayout? = null
    private var pcColorSelected = Color.WHITE
    private var pcColorUnselected = Color.WHITE
    private var textView: TextView? = null
    private var button: Button? = null
    private var webView: WebView? = null
    private var headerLayout: RelativeLayout? = null
    private var toastLayout: LinearLayout? = null
    private var acceptRadioButton: RadioButton? = null


    init {
        //this.window?.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.grey_color))
        this.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        val v = View.inflate(context, R.layout.layout_sidebar_popup_view,null)


        backgroundLayout = v.findViewById(R.id.backgroundLayout)
        alertView = v.findViewById(R.id.alertView)
        headerTextViewPopUp = v.findViewById(R.id.headerTextViewPopUp)
        bodyLayout = v.findViewById(R.id.bodyLayout)
        textView = v.findViewById(R.id.textView)
        webView = v.findViewById(R.id.webView)
        headerLayout = v.findViewById(R.id.headerLayout)

        //setUI()


        setContentView(v)

    }

    fun setUI(){

        bodyLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white_color)).cornerRadii(0,0,30,30).build()
        headerLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.black_color)).cornerRadii(50,50,0,0).build()
        webView?.background = DrawableBuilder().solidColor(Color.TRANSPARENT).cornerRadii(0,0,30,30).build()

    }

    fun setPropertiesPopUp(sideBarPopUpProperties: SideBarPopUpProperties?){

        //this.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        if (sideBarPopUpProperties?.popUpMainBackgroundColor != null){

            alertView?.setBackgroundColor(sideBarPopUpProperties.popUpMainBackgroundColor!!)

        }

        val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        params.setMargins(sideBarPopUpProperties?.popupLayoutLeftMargin!!,sideBarPopUpProperties.popupLayoutTopMargin,sideBarPopUpProperties.popupLayoutRightMargin,sideBarPopUpProperties.popupLayoutBottomMargin)

        backgroundLayout?.layoutParams = params

        if (sideBarPopUpProperties.headerTitleBackgroundColor != null){

            if (sideBarPopUpProperties.radius != null){
                headerTextViewPopUp?.background = DrawableBuilder().solidColor(Color.parseColor(sideBarPopUpProperties.headerTitleBackgroundColor)).cornerRadii(sideBarPopUpProperties.radius!!,sideBarPopUpProperties.radius!!,0,0).build()
            }else{
                headerTextViewPopUp?.setBackgroundColor(Color.parseColor(sideBarPopUpProperties.headerTitleBackgroundColor))
            }

        }

        if (sideBarPopUpProperties.headerTitleColor != null){
            headerTextViewPopUp?.setTextColor(Color.parseColor(sideBarPopUpProperties.headerTitleColor))
        }

        if (sideBarPopUpProperties.headerTitleGravity != null){
            headerTextViewPopUp?.gravity = sideBarPopUpProperties.headerTitleGravity!! or Gravity.CENTER
        }

        if (sideBarPopUpProperties.headerTitleIconLeft != null){
            headerTextViewPopUp?.setCompoundDrawablesWithIntrinsicBounds(sideBarPopUpProperties.headerTitleIconLeft!!,0,0,0)
        }

        if (sideBarPopUpProperties.headerTitleIconRight != null){
            headerTextViewPopUp?.setCompoundDrawablesWithIntrinsicBounds(0,0,sideBarPopUpProperties.headerTitleIconRight!!,0)
        }

        if (sideBarPopUpProperties.radius != null){

            bodyLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.white_color)).cornerRadii(0,0,sideBarPopUpProperties.radius!!,sideBarPopUpProperties.radius!!).build()
            headerLayout?.background = DrawableBuilder().solidColor(ContextCompat.getColor(context,R.color.transparent)).cornerRadii(sideBarPopUpProperties.radius!!,sideBarPopUpProperties.radius!!,0,0).build()

        }

        if (sideBarPopUpProperties.backgroundColor != null){
            this.window?.setBackgroundDrawable(ContextCompat.getDrawable(context, sideBarPopUpProperties.backgroundColor!!))
        }else{
            this.window?.setBackgroundDrawable(ContextCompat.getDrawable(context, R.color.grey_color))
        }




    }


    fun setVariables(triggerSideBar: TriggerSideBarResponse?, type: String?, viewType: String?, androidLinkMain: String?){

        this.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        if (type?.toLowerCase().equals("modal")){

            acceptRadioButton?.isChecked = false
            headerTextViewPopUp?.text = triggerSideBar?.hydraMember?.get(0)?.result?.title
            webView?.loadData(triggerSideBar?.hydraMember?.get(0)?.result?.content,null,null)

            headerTextViewPopUp?.setOnClickListener {

                mainSideBarMenu?.ad?.dismiss()

            }

            alertView?.setOnClickListener {

                mainSideBarMenu?.ad?.dismiss()

            }



        }

    }


}
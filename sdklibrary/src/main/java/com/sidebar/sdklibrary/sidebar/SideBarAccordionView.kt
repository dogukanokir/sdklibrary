package com.sidebar.sdklibrary.sidebar

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.sidebar.sdklibrary.R
import com.sidebar.sdklibrary.response.CollectionSection
import com.sidebar.sdklibrary.sidebar.AllViewProperties.MainAllProperties
import com.sidebar.sdklibrary.sidebar.AllViewProperties.SideBarAccordionProperties
import com.sidebar.sdklibrary.sidebar.accordionContentView.SideBarAccordionContentHtml
import com.sidebar.sdklibrary.sidebar.accordionContentView.SideBarAccordionContentSummary
import com.sidebar.sdklibrary.sidebar.singleDoubleColumnContentView.SideBarSingleDoubleColumnContentImage
import java.util.ArrayList

class SideBarAccordionView(context: Context,var mainSideBarMenu: MainSideBarMenu): RelativeLayout(context) {


    private var divider: View? = null
    private var accordionBgView: RelativeLayout? = null
    private var accordionTitle: TextView? = null
    private var accordionContentView: LinearLayout? = null
    private var drawableBgClose: Drawable? = null
    private var drawableBgOpen: Drawable? = null
    private var iconClose: Int? = null
    private var iconOpen: Int? = null
    private var leftIcon: Boolean? = null
    private var rightIcon: Boolean? = null
    private var titleColorOpen: Int? = null
    private var titleColorClose: Int? = null


    init {

        val view = LayoutInflater.from(context).inflate(R.layout.layout_sidebar_accordion_view,this)
        divider = view.findViewById(R.id.divider)
        accordionBgView = view.findViewById(R.id.accordionBgView)
        accordionTitle = view.findViewById(R.id.accordionTitle)
        accordionContentView = view.findViewById(R.id.accordionContentView)

        accordionBgView?.setOnClickListener {

            if (accordionContentView?.visibility == View.VISIBLE){
                accordionContentView?.visibility = View.GONE

                accordionBgView?.background = drawableBgClose

                if (titleColorOpen != null){
                    accordionTitle?.setTextColor(titleColorOpen!!)
                }

                if (leftIcon == true){
                    if (iconClose != null){
                        accordionTitle?.setCompoundDrawablesWithIntrinsicBounds(iconClose!!,0,0,0)
                    }
                }else if (rightIcon == true){
                    if (iconClose != null){
                        accordionTitle?.setCompoundDrawablesWithIntrinsicBounds(0,0,iconClose!!,0)
                    }
                }else{
                    if (iconClose != null){
                        accordionTitle?.setCompoundDrawablesWithIntrinsicBounds(iconClose!!,0,0,0)
                    }
                }


            }else{
                if (drawableBgOpen != null){
                    accordionBgView?.background = drawableBgOpen
                }

                if (iconOpen != null){
                    accordionTitle?.setCompoundDrawablesWithIntrinsicBounds(iconOpen!!,0,0,0)
                }

                if (titleColorClose != null){
                    accordionTitle?.setTextColor(titleColorClose!!)
                }

                accordionContentView?.visibility = View.VISIBLE
            }

        }

    }

    fun lineVisibilityGone(){

        divider?.visibility = View.GONE

    }

    fun lineViewColor(color: String){

        divider?.setBackgroundColor(Color.parseColor(color))

    }

    fun accordionTitleText(title: String?){

        accordionTitle?.text = title

    }


    fun setProperties(properties: SideBarAccordionProperties?){

        if (properties?.drawableBgClose != null){
            this.drawableBgClose = properties.drawableBgClose
            accordionBgView?.background = properties.drawableBgClose
        }

        if (properties?.drawableBgOpen != null){
            this.drawableBgOpen = properties.drawableBgOpen
        }


        if (properties?.iconClose != null){
            this.iconClose = properties.iconClose

            if (properties.left != null){
                leftIcon = properties.left
            }

            if (properties.right != null){
                rightIcon = properties.right
            }

            if (properties.left == true){
                accordionTitle?.setCompoundDrawablesWithIntrinsicBounds(iconClose!!,0,0,0)
            }else if (properties.right == true){
                accordionTitle?.setCompoundDrawablesWithIntrinsicBounds(0,0,iconClose!!,0)
            }else{
                accordionTitle?.setCompoundDrawablesWithIntrinsicBounds(iconClose!!,0,0,0)
            }
        }

        if (properties?.titleColorOpen != null){
            this.titleColorOpen = properties.titleColorOpen
            accordionTitle?.setTextColor(properties.titleColorOpen!!)
        }

        if (properties?.titleColorClose != null){
            this.titleColorClose = properties.titleColorClose
        }

        if (properties?.titleGravity != null){
            accordionTitle?.gravity = properties.titleGravity!!
        }

        if (properties?.iconOpen != null){
            this.iconOpen = properties.iconOpen
        }

        if (properties?.lineViewColor != null){

            divider?.setBackgroundColor(properties.lineViewColor!!)

        }

    }


    fun contentData(data: List<CollectionSection>?, mainAllProperties : MainAllProperties?){

        accordionContentView?.removeAllViews()

        for(i in 0..data?.size!!-1){

            when(data.get(i).lsection?.typeStr){

                "image" -> {

                    val view = SideBarSingleDoubleColumnContentImage(this.context)
                    view.setData(data.get(i).lsection?.mediaURL,data.get(i).lsection?.title)
                    view.setProperties(mainAllProperties?.sideBarSingleDoubleColumnContentImageProperties)

                    accordionContentView?.addView(view)

                    /*val view = SideBarAccordionContentImage(this.context)
                    if (i == 0){
                        view.lineVisibilityGone()
                    }
                    view.setTitleAndSubTitleData("Ders Bilgisi", "#7B3896","Derslere gelirken kitapları yanınıza almayı unutmayın.","#9959AB")
                    view.lineViewColor("#F0E6F3")
                    view.setImage("https://s3linspire.metodbox.com/1_QePO9QiIUCtpOipz-3219341858985e3b189109402fd06a55.png")
                    accordionContentView?.addView(view)*/

                }
                "summary" -> {

                    val view = SideBarAccordionContentSummary(this.context)
                    if (i == 0){
                        view.lineVisibilityGone()
                    }
                    view.setProperties(mainAllProperties?.sideBarAccordionContentSummaryProperties)
                    //view.lineViewColor("#F0E6F3")
                    view.setTitleAndSubTitleData(data.get(i).lsection?.title,data.get(i).lsection?.summary)
                    accordionContentView?.addView(view)

                }
                "html" -> {

                    val view = SideBarAccordionContentHtml(this.context,mainSideBarMenu)
                    view.webView?.visibility = View.VISIBLE
                    if (i == 0){
                        view.lineVisibilityGone()
                    }
                    //view.lineViewColor("#F0E6F3")
                    view.setProperties(mainAllProperties?.sideBarAccordionContentHtmlProperties)
                    view.setDataWebView(data.get(i).lsection?.link)
                    accordionContentView?.addView(view)

                }
                "rich" -> {

                    val view = SideBarAccordionContentHtml(this.context,mainSideBarMenu)

                    if (i == 0){
                        view.lineVisibilityGone()
                    }

                    if (data.get(i).lsection?.viewTypeStr.equals("popup")){
                        view.setDataPreviewImage(data.get(i).lsection?.previewImage,null,data.get(i).lsection?.html,data.get(i).lsection?.title,mainAllProperties)
                    }else{
                        view.webView?.visibility = View.VISIBLE
                        view.setHtml(data.get(i).lsection?.html)
                    }

                    //view.lineViewColor("#F0E6F3")
                    view.setProperties(mainAllProperties?.sideBarAccordionContentHtmlProperties)
                    accordionContentView?.addView(view)

                }
                else -> {



                }


            }

        }



    }



}
package com.sidebar.sdklibrary.request

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.sidebar.sdklibrary.network.*

class RequestSideBar(activity: AppCompatActivity?, fragment: Fragment?, apiKey: String?, groups: String?, showPlace: Int?, parentCol: Long?, exists: Boolean?, filter: JsonObject?, status: Int?, order: String?, site: Int?, listener: NetworkResponseListener<JsonArray>) {


    init {

        val request = RequestCreator.create<Services.GetSideBar>(Services.GetSideBar::class.java, Constants.NETWORKADDRESS.sideBarUrl)
        request.getSideBar(apiKey,groups,showPlace,parentCol,exists,filter,status,order,site).enqueue(
            NetworkResponse(listener, 1, activity, fragment)
        )

    }

}